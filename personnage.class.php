<?php 

class personnage {
	public $nom;
	public $hp;
	public $puissance;
	

	public function __construct($nom="", $hp="100", $puissance="10"){
		$this->nom 	= $nom;
		$this->hp 	= $hp;
		$this->puissance = $puissance;
	}

	public function Parler(){
		$nom = $this->nom;
		echo "<h1>Je suis $nom</h1>";
	}


}

trait sayen1{
	public function Transformer(){
		$this->nom 		 .= " Super Sayen";
		$this->hp 		 += 50;
		$this->puissance *= 2;
	}
}

trait sayen2{
	public function Transformer(){
		$this->nom 		 .= " Super Sayen 2";
		$this->hp 		 += 150;
		$this->puissance *= 3;
	}
}

class sayen extends personnage{
	use sayen1{
		Transformer as Rahhh;
	}

	public function __construct($nom=""){
		parent::__construct($nom);
	}
}