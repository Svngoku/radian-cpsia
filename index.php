<?php include("config/config.php");

?>

<!DOCTYPE html>
<html lang="fr">
  <?php Head("Accueil Application"); ?>
  <body>
    <?php Menu(); ?>
	
	<section class="container">
		<div class="row">
			<div class="col mt-5">
				<div class="jumbotron">
				  <h1 class="display-4">Tuto PHP &amp; Bootstrap</h1>
				  <p class="lead">Exemple de création d'une application PHP avec Mysql</p>
				  <hr class="my-4">
				  <p>Apprenez à créer vos propres applications from scratch</p>
				  <a class="btn btn-primary btn-lg" href="#" role="button">Télécharger</a>
				</div>
			</div>
		</div>
	</section>

    <?php Js(); ?>
  </body>
</html>