<?php include("../../config/config.php"); 
	$e = new students;

	//Mode Ajout
	if(isset($_GET["cas"]) && $_GET["cas"] == 1){
		$e->loadPost()->add();
		header("Location: index.php");
	}
?>

<!DOCTYPE html>
<html lang="fr">
  <?php Head("Gestion des élèves"); ?>
  <body>
    <?php Menu(); ?>
	
	<section class="container">
		<div class="row">
			<article class="col mt-5">
				<h1>Gestion des élèves <small>Liste</small></h1>
				<hr>
				
				<p>
					<a href="./add.php" class="btn btn-primary">Ajouter</a>
				</p>

				<?php students::Datagrid(); ?>
			</article>
		</div>
	</section>

    <?php Js(); ?>
  </body>
</html>