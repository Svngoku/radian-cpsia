<?php include("../../config/config.php"); ?>

<!DOCTYPE html>
<html lang="fr">
  <?php Head("Gestion des élèves"); ?>
  <body>
    <?php Menu(); ?>
	
	<section class="container">
		<div class="row">
			<article class="col-6 mt-5">
				<h1>Ajouter un élève <small>Formulaire</small></h1>
				<hr>
				
				<form action="index.php?cas=1" method="POST">
					<!-- <article class="form-group">
						<label>Prénom</label>
						<input type="text" name="prenom" placeholder="Ex: Philippe" class="form-control">
					</article> -->

					<article class="form-group">
						<label>Nom</label>
						<input type="text" name="nom" placeholder="Ex: Larrat" class="form-control">
					</article>

					<article class="form-group">
						<label>Age</label>
						<input type="number" min="0" max="150" name="age" placeholder="Ex: 29" class="form-control">
					</article>
					
					<hr>

					<p class="text-right">
						<a href="index.php" class="btn btn-light float-left">Retour</a>
						<button type="reset" class="btn btn-outline-secondary">Annuler</button>
						<button type="submit" class="btn btn-success">Enregistrer</button>
					</p>
				</form>
			</article>
		</div>
	</section>

    <?php Js(); ?>
  </body>
</html>