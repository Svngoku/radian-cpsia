<?php function Head($titre=""){ ?>
	<head>
	  <!-- Required meta tags -->
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	  <!-- Bootstrap CSS -->
	  <link rel="stylesheet" href="<?php echo URL_HOME ?>css/bootstrap.min.css" >

	  <title><?php echo $titre; ?></title>
	</head>
<?php }

function Js(){ ?>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="<?php echo URL_HOME ?>js/jquery.slim.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="<?php echo URL_HOME ?>js/bootstrap.min.js"></script>
<?php }

function Menu(){ ?>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="#">CPSIA 2020</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="<?php echo URL_HOME ?>modules/eleve">Eleves</a>
		  </li>
		  <li class="nav-item">
	        <a class="nav-link" href="<?php echo URL_HOME ?>modules/add">Add Eleve</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Formateurs</a>
	      </li>
	    </ul>
	    
	  </div>
	</nav>
<?php }