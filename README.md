# RADIAN ECOMMERCE AND DRIVE SYSTEM


## Getting Started

This project is a build with Flutter .
It's  SAAS by helping day to day routine of sending messages and emails.
The goal it's to automate the processes by adding simple functionality.

### Architectures for folders

- The lib folder:

```md
    lib/
        |- main.dart
        |- routes.dart
        |- screens/
        |- util/
        |- widgets/
        |- data/
        |- services/
```

- SCREENS : Contains the screens of  the application. All files from here get imported into routes.dart

```md
screens/
    |- auth
    |- auth.dart
    |- index.dart
    |- widgets
        |- Button
            |- button.dart
            |- index.dart
        |- Field
            |- Field.dart
            |- index.dart
    |- home
        |- home.dart
        |- index.dart
        |- widgets
```

- UTIL: Util folder contains the business logic of your application

```md
util/
    |- date_utils.dart
    |- format_utils.dart
```

- WIDGETS : contain the common used across the application ( button, TextField )

```md
widgets/
    |- app_button/
        |- app_button.dart
        |- index.dart
```

- DATA : contains the picture once you integrate redux(or other) store into the application. ( example )

```md
data/
    |- auth
        |- access_token
            |- actions.dart
            |- reducer.dart
        |- refresh_token
            |- actions.dart
            |- reducer.dart
        |- reducer.dart

```

### Libraries & Services

- php 7

- graphql

- postgresql

- microservice architecture

## TO DO

- Database stable

- CRUD product, inventory, rapport, users, fournisseur

- Deploy to docker

- graphql api call backend

