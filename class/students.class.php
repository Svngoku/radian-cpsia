<?php 

class students extends projet_global{
	
	public $nom;
	public $prenom;
	public $age;

	public function __construct(){
		parent::__construct();
		$this->nom 		= "";
		$this->prenom 	= "";
		$this->age 		= 0;
	}

	public static function Datagrid(){ 
		$obj = new self;
		$datas = $obj->getAll();

	?>
		
		<section class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Prenom &amp; Nom</th>
						<th>Age</th>
						<th>Modifier</th>
						<th>Supprimer</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($datas as $key => $ligne) { ?>
						<tr>
							<td><?php echo $ligne["id"]; ?></td>
							<td><?php echo $ligne["prenom"]; ?> <?php echo strtoupper($ligne["nom"]); ?></td>
							<td><?php echo $ligne["age"]; ?></td>
							<td><button class="btn btn-warning">Modifier</button></td>
							<td><button class="btn btn-danger">Suppression</button></td>
						</tr>
					<?php } ?>
					
				</tbody>
			</table>
		</section>
	<?php }
}